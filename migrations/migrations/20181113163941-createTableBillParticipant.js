'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'BillParticipant', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        billId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false,
        },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false,
        },
        amount: {
          type: Sequelize.FLOAT,
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },


      },
    ).then(() => {
      return queryInterface
        .addConstraint('BillParticipant', ['billId'], {
          type: 'foreign key',
          name: 'FK_BillParticipant_Bill',
          references: {
            table: 'Bill',
            field: 'id'
          }
        }).then(() => {
          return queryInterface
            .addConstraint('BillParticipant', ['userId'], {
              type: 'foreign key',
              name: 'FK_BillParticipant_User',
              references: {
                table: 'User',
                field: 'id'
              }
            })
        })
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('BillParticipant')
  }
};