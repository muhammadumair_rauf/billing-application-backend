'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'User', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        username: {
          type: Sequelize.STRING,
        },
        name: {
          type: Sequelize.STRING,
        },
        password: {
          type: Sequelize.STRING,
        },
        fbLogin: {
          type: Sequelize.BOOLEAN,
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },

      },
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('User')
  }
};