'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'UserFriend', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        senderId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false,
        },
        recieverId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false,
        },
        status: {
          type: Sequelize.STRING,
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },


      },
    ).then(() => {
      return queryInterface
        .addConstraint('UserFriend', ['senderId'], {
          type: 'foreign key',
          name: 'FK_UserFriend_User_senderId',
          references: {
            table: 'User',
            field: 'id'
          }
        }).then(() => {
          return queryInterface
            .addConstraint('UserFriend', ['recieverId'], {
              type: 'foreign key',
              name: 'FK_UserFriend_User_recieverId',
              references: {
                table: 'User',
                field: 'id'
              }
            })
        })
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UserFriend')
  }
};