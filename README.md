# Billing App Backend

This project was generated with Node,Express and Sequelize(ORM) http://docs.sequelizejs.com/ . 
Database that I used is MSSQL. 

## How To Run
* First install node dependencies by running the command. `npm install`. 
* Update the database username, password in the file **local-setting.ts** placed at 
* src
    * modules
        * base
            * conf
```
export const CONNECTION_STRING = {
  username: "sa", // Your username
  password: "Linked2P@ss", // Your Database password
  database: "billing_db", // Your Database
  host: "127.0.0.1",
  dialect: "mssql", // For MySql 'mysql'
  logging: true,
  storage: ':memory:'

};
export const DEV_ENV = {
  frontendURL: 'https://localhost:4200',
  backendURL: 'http://localhost:3000',
}
export const PROD_ENV = {
  frontendURL: '',
  backendURL: '',
}

``` 
## Create table from script
* I have placed table script named **createTableScript.sql** You can create table from this or you can run the migrations as describe below.

### How To Run Migrations

* Update the **config.json** placed at
* migrations
    * conf

```
{
  "development": {
    "username": "sa",
    "password": "Linked2P@ss",
    "database": "billing_db",
    "host": "127.0.0.1",
    "dialect": "mssql",
    "logging": true,
    "seederStorage": "sequelize"

  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mssql"
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mssql"
  }
}
```

* For running migration you have to install Sequelize-CLI Globally from here https://www.npmjs.com/package/sequelize-cli go to the folder migrations and run the following command here `sequelize db:migrate`. For more info http://docs.sequelizejs.com/manual/tutorial/migrations.html.

* Run `npm start` to run the backend project. It will run on http://localhost:3000
* I have used grunt as a compiler to compile TypeScript file.
