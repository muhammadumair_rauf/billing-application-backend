USE [billing_db]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 14-Nov-18 9:44:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](255) NULL,
	[amount] [float] NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillParticipant]    Script Date: 14-Nov-18 9:44:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillParticipant](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[billId] [int] NOT NULL,
	[userId] [int] NOT NULL,
	[amount] [float] NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 14-Nov-18 9:44:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[password] [nvarchar](255) NULL,
	[fbLogin] [bit] NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserFriend]    Script Date: 14-Nov-18 9:44:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserFriend](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[senderId] [int] NOT NULL,
	[recieverId] [int] NOT NULL,
	[status] [nvarchar](255) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BillParticipant] ADD  DEFAULT ((0)) FOR [billId]
GO
ALTER TABLE [dbo].[BillParticipant] ADD  DEFAULT ((0)) FOR [userId]
GO
ALTER TABLE [dbo].[UserFriend] ADD  DEFAULT ((0)) FOR [senderId]
GO
ALTER TABLE [dbo].[UserFriend] ADD  DEFAULT ((0)) FOR [recieverId]
GO
ALTER TABLE [dbo].[BillParticipant]  WITH CHECK ADD  CONSTRAINT [FK_BillParticipant_Bill] FOREIGN KEY([billId])
REFERENCES [dbo].[Bill] ([id])
GO
ALTER TABLE [dbo].[BillParticipant] CHECK CONSTRAINT [FK_BillParticipant_Bill]
GO
ALTER TABLE [dbo].[BillParticipant]  WITH CHECK ADD  CONSTRAINT [FK_BillParticipant_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[BillParticipant] CHECK CONSTRAINT [FK_BillParticipant_User]
GO
ALTER TABLE [dbo].[UserFriend]  WITH CHECK ADD  CONSTRAINT [FK_UserFriend_User_recieverId] FOREIGN KEY([recieverId])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[UserFriend] CHECK CONSTRAINT [FK_UserFriend_User_recieverId]
GO
ALTER TABLE [dbo].[UserFriend]  WITH CHECK ADD  CONSTRAINT [FK_UserFriend_User_senderId] FOREIGN KEY([senderId])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[UserFriend] CHECK CONSTRAINT [FK_UserFriend_User_senderId]
GO
