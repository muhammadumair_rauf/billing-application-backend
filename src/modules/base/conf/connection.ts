import { Sequelize } from 'sequelize-typescript';

import { CONNECTION_STRING } from './local-settings';

/**
 * Import from Home Module
 * 
 */
import { User, UserFriend, Bill, BillParticipant } from '../../home';


export class Connection {
  sequelize: Sequelize;
  constructor() { }
  public createConnection(): Sequelize {
    /** Instantiating Sequelize instance for creating connection */
    this.sequelize = new Sequelize(CONNECTION_STRING);

    this.sequelize
      .authenticate()
      .then(() => {
        // console.log('Connection has been established successfully.');
      })
      .catch(err => {
        // console.error('Unable to connect to the database:', err);
      });
    this.sequelize.addModels([
      User,
      UserFriend,
      Bill,
      BillParticipant
    ]);
    return this.sequelize;
  }
}
