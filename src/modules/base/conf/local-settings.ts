export const CONNECTION_STRING = {
  username: "sa", // Your username
  password: "Linked2P@ss", // Your Database password
  database: "billing_db", // Your Database
  host: "127.0.0.1",
  dialect: "mssql", // For MySql 'mysql'
  logging: true,
  storage: ':memory:'

};
export const DEV_ENV = {
  frontendURL: 'https://localhost:4200',
  backendURL: 'http://localhost:3000',
}
export const PROD_ENV = {
  frontendURL: '',
  backendURL: '',
}
