export const CONFIGURATIONS = {

  appName: 'Billing App',

  connection: null,

  SECRET: 'billing_app_secret',

  // following are public urls.
  PUBLIC_URLS: [
    '/home/users/login',
    '/home/users/create'
  ],


  environment: {
    frontendURL: 'https://localhost:4200',
    backendURL: 'http://localhost:3000',
  },

  FRIEND_STATUS: {
    sent: { title: 'Friend Request Sent', value: 'sent' },
    friend: { title: 'Friend', value: 'friend' }
  }
}
