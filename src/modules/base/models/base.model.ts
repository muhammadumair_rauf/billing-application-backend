import { Sequelize } from 'sequelize-typescript';
import { Connection, CONFIGURATIONS } from '..';


export class BaseModel {
  public sequelize = Sequelize;
  public sequelizeModel;
  protected connection;

  constructor(model) {
    this.sequelizeModel = model;
    this.openConnection();
  }

  protected openConnection() {
    if (!CONFIGURATIONS.connection) {
      console.log('-----------------------------------------------------------');
      console.log('Db Connection is created (' + new Date() + ')');
      console.log('-----------------------------------------------------------');
      CONFIGURATIONS.connection = new Connection().createConnection();
    }

    this.connection = CONFIGURATIONS.connection;
  }

  protected closeConnection() {

    this.connection.close();
    CONFIGURATIONS.connection = null;
  }

  /**
   * Find single record by id
   * @param id
   */
  find(id, attributes?, include?, order?) {
    return this.findByCondition(attributes, { id: id }, include, order);
  }

  findAndCountAll(attributes?, conditions?, include?, order?, options?) {

    let args = BaseModel.qry2SequelizeOptions(options);

    return this.sequelizeModel.findAndCountAll(
      this.sequelizeQueryBuilder(attributes, conditions, include, order, args)
    );
  }

  /**
   * Find single record by specified condition
   * @param attributes
   */
  findByCondition(attributes, conditions, include?, order?) {

    return this.sequelizeModel.findOne(
      this.sequelizeQueryBuilder(attributes, conditions, include, order)
    );
  }

  /**
   * Find all records with specified attributes
   * @param attributes
   */
  findAll(attributes?, conditions?, include?, order?) {
    return this.findAllByConditions(attributes, conditions, include, order);
  }

  /**
   * Find all records with specified attributes and conditions
   * @param attributes
   */
  findAllByConditions(attributes, conditions, include?, order?) {

    return this.sequelizeModel.findAll(
      this.sequelizeQueryBuilder(attributes, conditions, include, order)
    );
  }

  /**
   * Update a record for given id
   * @param item
   * @param id
   */
  update(id, item) {
    return this.updateByCondition(item, { id: id })
  }

  /**
   * Update a record for given id
   * @param item
   * @param id
   */
  updateByCondition(item, conditions) {
    return this.sequelizeModel.update(item, { where: conditions });
  }

  /**
   * Create a new record
   * @param item
   */
  create(item) {


    return this.sequelizeModel.create(item);
  }

  /**
   * Count all records
   */
  // count() {
  //   return this.sequelizeModel.count();
  // }
  count(condition?, includes?) {

    return this.sequelizeModel.count(
      this.sequelizeQueryBuilder(null, condition, includes)
    );

  }

  sum(column, condition?, includes?) {

    return this.sequelizeModel.sum(column, this.sequelizeQueryBuilder(null, condition, includes));

  }

  /**
   * Delete a record against an id
   * @param id
   */
  delete(id) {
    return this.sequelizeModel.destroy({ where: { id: id } });
  }




  /**
   * To prepare the sequelize query.
   * attributes: ['a1', 'a2']
   * condition: {a: b, c: d}
   * includes : [{YOUR_INCLUDE}]
   * order: [['updatedAt', 'DESC']]
   *
   * @param attributes any
   * @param condition any
   */
  protected sequelizeQueryBuilder(attributes?, condition?, include?, order?, options?) {
    let obj = {};

    if (attributes) {
      obj['attributes'] = attributes;
    }

    obj['where'] = this.conditionBuilder(condition);

    if (include) {
      obj['include'] = include;
    }

    if (order) {
      obj['order'] = order;
    }

    if (options && options['offset']) {
      obj['offset'] = options['offset'];
    }

    if (options && options['limit']) {
      obj['limit'] = options['limit'];
    }

    return obj;
  }


  /**
   * Build conditon object
   * 
   * @param condition 
   */
  public conditionBuilder(condition?) {
    return BaseModel.cb(condition);
  }

  /**
   * 
   * @param condition 
   */
  public static cb(condition?) {

    let updatedCondition = (condition ? condition : {});


    return updatedCondition;
  }


  /**
     * Convert query sting object to sequelize options object
     * 
     */
  static qry2SequelizeOptions(query) {

    let options = {};
    if (query) {

      if (query['offset']) {
        options['offset'] = Number(query['offset'])
      }

      if (query['limit']) {
        options['limit'] = Number(query['limit'])
      }

    }


    return options;
  }

}
