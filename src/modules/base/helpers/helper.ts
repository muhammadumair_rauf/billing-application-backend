import { Sequelize } from 'sequelize-typescript';
import * as bcrypt from 'bcrypt';

export class Helper {

    // used to enctypt string
    static SALTROUNDS = 10;

    /**
     * Query to Sequelize Condition
     * 
     * This method is for converting the provided query string object into sequelize condition.
     * 
     * f_ prefix means its for filtering so we have to use equals
     * s_ prefix means its for searching so we can use like
     * 
     * @param query object {}
     * @returns condition object Sequelize object
     */
    static query2Condition(query) {

        let condition = {};

        Object.keys(query).forEach((key) => {

            if (key.indexOf('f_') > -1) {

                let k = key.split('f_')[1];
                condition[k] = query[key]
            }
            else if (key.indexOf('s_') > -1) {

                let k = key.split('s_')[1];

                if (typeof query[key] == "string") {
                    condition[k] = { [Sequelize.Op.like]: '%' + query[key] + '%' }
                }
                else {
                    condition[k] = query[key]
                }
            }
            else {
                condition[key] = query[key]
            }
        })

        return condition;
    }


    /**
     * Encrypt str with bcrypt
     * @param str 
     */
    static encrypt(str) {
        return bcrypt.hash(str, this.SALTROUNDS);
    }

    /**
     * Verify password
     * @param password 
     * @param hashedPassword 
     */
    static verifyPassword(password, hashedPassword) {
        return bcrypt.compare(password, hashedPassword);
    }

    static getCurrentMonthStartEndDates() {
        let d = new Date();
        let month = d.getMonth() + 1;
        let year = d.getFullYear();

        let lastDayDate = new Date(year, month, 0);
        let lastDayOfMonth = lastDayDate.getDate();

        let start_date = `${year}-${month}-01`;
        let end_date = `${year}-${month}-${lastDayOfMonth}`;

        return {
            start_date: start_date,
            end_date: end_date
        }
    }

}