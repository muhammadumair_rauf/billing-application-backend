export interface IUserModel {
  id?: number;
  username?: string;
  password?: string;
}
