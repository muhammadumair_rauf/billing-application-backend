
import { BaseModel } from '../../base';

import { Bill } from '..';
import { Promise } from 'bluebird';
import { BillParticipantModel } from './bill-participant.model';
import { BillParticipant } from './schema/bill-participant';
import { User } from './schema/user';

export class BillModel extends BaseModel {

  constructor() {
    super(Bill);
  }

  createWithRelatedData(item) {
    let billItem = { amount: item['amount'], title: item['title'] }
    let perPersonAmount = item['amount'] / item['userIds'].length;
    return this.create(billItem).then(bill => {
      return Promise.each(item['userIds'], userId => {

        return new BillParticipantModel().create({ userId: userId, billId: bill['id'], amount: perPersonAmount })

      })
    })
  }

  findAllWithRelatedData() {
    let include = [{
      model: BillParticipant, as: 'billParticipants', required: false,
      include: [{ model: User, as: 'participant', required: true }]
    }]
    return this.findAll(null, null, include);
  }

}
