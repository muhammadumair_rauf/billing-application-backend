import { Sequelize } from 'sequelize-typescript';
import * as _ from 'lodash';
import * as jwt from 'jsonwebtoken';

import { BaseModel, CONFIGURATIONS } from '../../base';
import { IUserModel } from './interfaces/IUserModel';
import { ErrorHandler } from '../../base/conf/error-handler';
import { Helper } from '../../base/helpers/helper';

import { User } from '..';
import { Promise } from 'bluebird';
import { UserFriend } from './schema/user-friend';

export class UserModel extends BaseModel {

  constructor() {
    super(User);
  }

  /**
   * Validate user to login and return jwt token and permissions
   * 
   * @param item 
   */
  public login(item: IUserModel) {

    return super.findByCondition(['id', 'username', 'password'], { username: item.username }).then(res => {

      if (res) {

        let userRes = <IUserModel>res;

        return Helper.verifyPassword(item.password, userRes.password).then(match => {

          if (res && match) {

            let token = jwt.sign({ id: res['id'], username: res['username'], iat: Math.floor(Date.now() / 1000) - 30 }, CONFIGURATIONS.SECRET);

            let result = {
              id: userRes.id, username: userRes.username,
              token: token,
            }
            return result;

          } else {
            // Return invalid credentials message
            return ErrorHandler.invalidLogin;
          }
        });

      } else {

        return ErrorHandler.invalidLogin;

      }
    });
  }



  /**
   * Validate username i.e email to create and update user
   * 
   * @param email 
   * @param id 
   */
  public validateUniqueEmail(email: string, id?: number, ) {

    // if id exists (i.e update scenario validate username except current user)
    if (id) {

      return super.findByCondition(['id'], { id: { [Sequelize.Op.not]: id }, username: email });

    } else {

      return super.findByCondition(['id'], { username: email });

    }
  }

  findAllWithRequests(senderId: number) {
    let includeObj = [
      { model: UserFriend, as: 'recievers', where: BaseModel.cb({ senderId: senderId }), required: false },
      { model: UserFriend, as: 'senders', where: BaseModel.cb({ recieverId: senderId }), required: false }
    ];

    return super.findAll(['id', 'username', 'name'], { id: { [Sequelize.Op.not]: senderId } }, includeObj);
  }

  /**
   * Insert user record into database after validate unique username i.e email
   * 
   * @param item 
   */
  public createUser(item) {

    return this.validateUniqueEmail(item.username).then(result => {

      if (result) {

        return ErrorHandler.duplicateEmail;

      } else {

        return Helper.encrypt(item.password).then(hashedPassword => {

          item.password = hashedPassword;
          return super.create(item);

        });
      }
    });
  }
}
