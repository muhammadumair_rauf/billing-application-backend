import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { User } from './user';
/**
 * Importing related Models
 */

@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns

export class UserFriend extends Model<UserFriend> {

  @ForeignKey(() => User)
  @Column senderId: number;

  @ForeignKey(() => User)
  @Column recieverId: number;

  @Column status: string;

  /** 
   * BelongsTo Relationships
   */
  @BelongsTo(() => User, { foreignKey: 'senderId' })
  sender: User;

  @BelongsTo(() => User, { foreignKey: 'recieverId' })
  reciever: User;


}
