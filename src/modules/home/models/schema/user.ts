import { Table, Column, Model, HasMany, HasOne } from 'sequelize-typescript';
import { UserFriend } from './user-friend';
/**
 * Importing related Models
 */

@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns

export class User extends Model<User> {

  @Column username: string;

  @Column name: string;

  @Column password: string;

  @Column fbLogin: boolean;

  /**
   * Has Many Rekationships
   */
  @HasMany(() => UserFriend, { foreignKey: 'senderId' })
  senders: UserFriend[];

  @HasMany(() => UserFriend, { foreignKey: 'recieverId' })
  recievers: UserFriend[];


}
