import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { BillParticipant } from './bill-participant';
/**
 * Importing related Models
 */

@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns

export class Bill extends Model<Bill> {

  @Column title: string;

  @Column amount: number;

  /**
   * Has Many Rekationships
   */
  @HasMany(() => BillParticipant, { foreignKey: 'billId' })
  billParticipants: BillParticipant[];



}
