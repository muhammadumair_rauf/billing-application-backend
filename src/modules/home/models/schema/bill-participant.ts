import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { User } from './user';
import { Bill } from './bill';
/**
 * Importing related Models
 */

@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns

export class BillParticipant extends Model<BillParticipant> {

  @ForeignKey(() => User)
  @Column userId: number;

  @ForeignKey(() => Bill)
  @Column billId: number;

  @Column amount: number;

  /** 
   * BelongsTo Relationships
   */
  @BelongsTo(() => User, { foreignKey: 'userId' })
  participant: User;

  @BelongsTo(() => User, { foreignKey: 'billId' })
  bill: Bill;


}
