import * as _ from 'lodash';

import { BaseModel } from '../../base';

import { UserFriend, User } from '..';
import { CONFIGURATIONS } from '../../base/conf/configurations';
import { Sequelize } from 'sequelize-typescript';

export class UserFriendModel extends BaseModel {

  constructor() {
    super(UserFriend);
  }
  findFriends(reciverId: number, status: string) {
    let includeObj = [
      { model: User, as: 'reciever', attributes: ['id', 'name', 'username'], required: true },
      { model: User, as: 'sender', attributes: ['id', 'name', 'username'], required: true }
    ];

    return super.findAll(['id', 'recieverId', 'senderId'], {
      $or: [
        {
          recieverId:
          {
            $eq: reciverId
          },
          senderId: { [Sequelize.Op.not]: reciverId },
        },
        {
          senderId:
          {
            $eq: reciverId
          },
          recieverId: { [Sequelize.Op.not]: reciverId },
        },

      ],

      status: status,


    }, includeObj);
  }

  findFriendRequests(reciverId: number, status: string) {
    let includeObj = [
      { model: User, as: 'reciever', attributes: ['id', 'name', 'username'], required: true },
      { model: User, as: 'sender', attributes: ['id', 'name', 'username'], required: true }
    ];

    return super.findAll(['id', 'recieverId', 'senderId'], { recieverId: reciverId, status: status }, includeObj);
  }

  public friendRequestAction(id: number, status: string) {
    if (status === CONFIGURATIONS.FRIEND_STATUS.friend.value) {
      return this.update(id, { status: CONFIGURATIONS.FRIEND_STATUS.friend.value })
    } else {
      return this.delete(id);
    }
  }
}
