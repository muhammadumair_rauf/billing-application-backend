import * as _ from 'lodash';

import { BaseModel } from '../../base';

import { BillParticipant } from '..';

export class BillParticipantModel extends BaseModel {

  constructor() {
    super(BillParticipant);
  }

}
