import * as express from 'express';
import * as _ from 'lodash';

import { ErrorHandler } from '../../base/conf/error-handler';
import { UserFriendModel } from '..';

export class UserFriendsController {
  constructor() { }


  /**
 * Create User Friend
 *
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;
    new UserFriendModel().create(item).then(result => {

      if (result && !result['error']) {

        res.json(result);

      } else {

        ErrorHandler.send(result, res, next);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * This function wil find the friends
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findFriends(req: express.Request, res: express.Response, next: express.NextFunction) {

    let reciverId = req.params.id;
    let status = req.params.status;
    new UserFriendModel().findFriends(reciverId, status).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * This function will find the friend request
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findFriendRequests(req: express.Request, res: express.Response, next: express.NextFunction) {

    let reciverId = req.params.id;
    let status = req.params.status;
    new UserFriendModel().findFriendRequests(reciverId, status).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * will accept or reject the friend requests
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  friendRequestAction(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;
    let status = req.params.status;
    new UserFriendModel().friendRequestAction(id, status).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

}
