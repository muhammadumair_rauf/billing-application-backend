import * as express from 'express';
import * as _ from 'lodash';

import { ErrorHandler } from '../../base/conf/error-handler';
import { UserModel } from '..';

export class UsersController {
  constructor() { }

  /**
   * Login
   *
   * @param req {User}
   */
  login(req: express.Request, res: express.Response, next: express.NextFunction) {

    new UserModel().login(req.body).then(result => {

      if (result && !result['error']) {

        res.json(result);

      } else {

        ErrorHandler.send(result, res, next);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }


  /**
   *Find list f user i.e People
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  list(req: express.Request, res: express.Response, next: express.NextFunction) {

    let senderId = req.params.id;
    new UserModel().findAllWithRequests(senderId).then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }


  /**
 * Create user
 *
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;

    new UserModel().createUser(item).then(result => {

      if (result && !result['error']) {

        res.json(result);

      } else {

        ErrorHandler.send(result, res, next);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

}
