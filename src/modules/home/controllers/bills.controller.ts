import * as express from 'express';
import * as _ from 'lodash';

import { ErrorHandler } from '../../base/conf/error-handler';
import { BillModel } from '..';

export class BillsController {
  constructor() { }


  /**
 * Create Bill
 *
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;
    new BillModel().createWithRelatedData(item).then(result => {

      if (result && !result['error']) {

        res.json(result);

      } else {

        ErrorHandler.send(result, res, next);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

  /**
   * Find the list of bills
  *
  * @param req express.Request
  * @param res express.Response
  * @param next express.NextFunction
  */
  list(req: express.Request, res: express.Response, next: express.NextFunction) {
    new BillModel().findAllWithRelatedData().then(result => {

      res.json(result);

    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }

}
