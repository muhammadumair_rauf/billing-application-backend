import * as express from 'express';
import * as _ from 'lodash';

import { ErrorHandler } from '../../base/conf/error-handler';
import { BillParticipantModel } from '..';

export class BillParticipantsController {
  constructor() { }

  /**
 * Create BillParticipant
 *
 * @param req express.Request
 * @param res express.Response
 * @param next express.NextFunction
 */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;
    new BillParticipantModel().create(item).then(result => {

      if (result && !result['error']) {

        res.json(result);

      } else {

        ErrorHandler.send(result, res, next);

      }
    }).catch(err => {

      ErrorHandler.sendServerError(err, res, next);

    });
  }


}
