import { Router } from 'express';

import { UsersController } from '..';

/**
 * / route
 *
 * @class User
 */
export class UserRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class UserRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class UserRoute
   * @method create
   *
   */
  public create() {
    let controller = new UsersController();

    this.router.route('/home/users/list/:id').get(controller.list);

    this.router.route('/home/users/login').post(controller.login);

    this.router.route('/home/users/create').post(controller.create);


  }
}
