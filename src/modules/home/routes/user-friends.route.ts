import { Router } from 'express';

import { UserFriendsController } from '..';

/**
 * / route
 *
 * @class UserFriend
 */
export class UserFriendsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class UserFriendRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class UserFriendRoute
   * @method create
   *
   */
  public create() {
    let controller = new UserFriendsController();

    this.router.route('/home/userFriends/create').post(controller.create);
    this.router.route('/home/userFriends/findFriends/:id/:status').get(controller.findFriends);
    this.router.route('/home/userFriends/findFriendRequests/:id/:status').get(controller.findFriendRequests);
    this.router.route('/home/userFriends/friendRequestAction/:id/:status').get(controller.friendRequestAction);

  }
}
