import { Router } from 'express';

import { BillParticipantsController } from '..';

/**
 * / route
 *
 * @class BillParticipant
 */
export class BillParticipantsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BillParticipantRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class BillParticipantRoute
   * @method create
   *
   */
  public create() {
    let controller = new BillParticipantsController();

    this.router.route('/home/billParticipants/create').post(controller.create);

  }
}
