import { NextFunction, Request, Response, Router } from 'express';
import {
  UserRoute,
  UserFriendsRoute,
  BillsRoute,
  BillParticipantsRoute
} from '../..';

/**
 * / route
 *
 * @class BaseRoute
 */
export class HomeBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new UserRoute(this.router);
    new UserFriendsRoute(this.router);
    new BillsRoute(this.router);
    new BillParticipantsRoute(this.router);
  }
}
