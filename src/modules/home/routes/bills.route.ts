import { Router } from 'express';

import { BillsController } from '..';

/**
 * / route
 *
 * @class Bill
 */
export class BillsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BillRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class BillRoute
   * @method create
   *
   */
  public create() {
    let controller = new BillsController();

    this.router.route('/home/bills/create').post(controller.create);
    this.router.route('/home/bills/list').get(controller.list);

  }
}
